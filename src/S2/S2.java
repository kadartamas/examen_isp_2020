package S2;

public class S2 implements Runnable{
    String name;

    S2(String name)
    {
        this.name=name;
    }
    public void run() {
        try {
            for(int i=0; i<7; i++)
            {
                System.out.println(name + " - " + java.time.LocalTime.now());
                Thread.sleep(1000);
            }
        }catch (Exception e){
            System.out.println("Error");
        }
    }

    public static void main(String[] args)
    {
        Thread BThread1 = new Thread(new S2("BThread1"));
        Thread BThread2 = new Thread(new S2("BThread2"));
        Thread BThread3 = new Thread(new S2("BThread3"));
        BThread1.start();
        BThread2.start();
        BThread3.start();
    }
}
