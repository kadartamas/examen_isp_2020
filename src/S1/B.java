package S1;

import java.util.ArrayList;

public class B extends A {
    String param;
    D d = new D();
    C[] c = new C[2];
    private ArrayList<E> e = new ArrayList<E>();

    void x() {
        param += " ";
    }

    void y() {
        param += "\n";
    }
}

class A {

}

class C {
}

class D {
}

class E {
}

class U {
    void func(B b){}
}

